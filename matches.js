const readlineSync = require('readline-sync');

function humanPlay(player, remainingSticks) {
    let takenSticks;
    do {
        // l'utilisateur saisi le nombre qu'il veut prendre (entre 1 et 3) et on le stocke dans une variable
        takenSticks = parseInt(readlineSync.question(player.name + ": how many sticks do you want to take [1, " + Math.min(3, remainingSticks-1) + "] ? "));
    // tant que the number is < 1 or > 3 or > batonnets restants
    } while (takenSticks < 1 || takenSticks > Math.min(3, remainingSticks-1));
    return takenSticks;
}

function randomPlay(player, remainingSticks) {
    return Math.ceil(Math.random()*Math.min(3, remainingSticks-1));
}

function cleverPlay(player, remainingSticks) {
    let r = remainingSticks % 4;
    if (r == 0)
        return 3;
    else if (r == 1)
        return Math.ceil(Math.random()*Math.min(3, remainingSticks-1));
    else
        return r-1;
}


// define remaining sticks
let remainingSticks = 10 + Math.floor(Math.random()*10);

// define players
let players = [
    {
        name: readlineSync.question("First player name ? "),
        playerType: parseInt(readlineSync.question("First player type (1: human, 2: random, 3: clever) ? "))
    },
    {
        name: readlineSync.question("Second player name ? "),
        playerType: parseInt(readlineSync.question("Second player type (1: human, 2: random, 3: clever) ? "))
    }
]

// define current player
let currentPlayer = 0;

// while there are more than one remaining stick
while (remainingSticks > 1) {
    // print remaining sticks
    for (let i=0; i<remainingSticks; ++i)
        process.stdout.write("| ");
    process.stdout.write("(" + remainingSticks + ")\n");
    // get taken sticks from player
    let takenSticks;
    switch (players[currentPlayer].playerType) {
        case 1:
            takenSticks = humanPlay(players[currentPlayer], remainingSticks);
            break;
        case 2:
            takenSticks = randomPlay(players[currentPlayer], remainingSticks);
            console.log(players[currentPlayer].name + " take " + takenSticks + " sticks")
            break;
        case 3:
            takenSticks = cleverPlay(players[currentPlayer], remainingSticks);
            console.log(players[currentPlayer].name + " take " + takenSticks + " sticks")
            break;
    }

    // updating remaining sticks
    remainingSticks = remainingSticks - takenSticks;
    // updating current player
    currentPlayer = currentPlayer == 0 ? 1 : 0;
}

// end game message
console.log(players[currentPlayer].name + " lose");
